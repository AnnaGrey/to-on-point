window.addEventListener('scroll', function() {
    let scrollY = window.pageYOffset;
    let elem = document.getElementById('next');
    if (scrollY >= 768) {
        let num = (1536 - scrollY) * 0.001;
        elem.style.opacity = num;
    }
    else {
        elem.style.opacity = '1';
    }

    if (10 < scrollY <= 768) {
        let num = (768 - scrollY) * 0.001;
        let indicator = 1 - num;
        if (indicator < 0.24) {
            indicator = 0;
        }
        document.getElementById('up').style.opacity = num;
        document.getElementById('central').style.opacity = indicator;
    }
    if (768 < scrollY <= 1536) {
        let num = (1536 - scrollY) * 0.001;
        let indicator = 1 - num;
        let x = scrollY / 768;
        if (x > 1) {
            x = num;
        }
        if (indicator < 0.24) {
            indicator = 0;
        }
        document.getElementById('down').style.opacity = indicator;
        document.getElementById('central').style.opacity = x;
    }
})



